/**
 * @file auth.module.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/26/2018
 * (c): 2018
 */
import {NgModule} from "@angular/core";
import {LoginFormComponent} from "@common/components/auth/login-form.component";


@NgModule({

  declarations:[
    LoginFormComponent
  ]
})
export class AuthModule {

}
