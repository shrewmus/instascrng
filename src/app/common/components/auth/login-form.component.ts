/**
 * @file login-form.component.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/26/2018
 * (c): 2018
 */
import {Component} from "@angular/core";

@Component({
  selector: 'in-login',
  templateUrl: 'in-login.html'
})
export class LoginFormComponent {

}
