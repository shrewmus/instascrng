/**
 * @file account-user.model.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/27/2018
 * (c): 2018
 */
import {Fillable} from "@common/fillable";

export class AccountUserModel implements Fillable {

  id: number;
  firstName: string;
  lastName!:string;
  authToken!: string;


  fill(inputObject: any) {}

}
