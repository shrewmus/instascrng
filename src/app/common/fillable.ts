/**
 * @file fillable.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 09/27/2018
 * (c): 2018
 */

export interface Fillable {
  fill(inputObject: any);
}
