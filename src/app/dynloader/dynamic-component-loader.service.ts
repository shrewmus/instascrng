/**
 * @file dynamic-component-loader.service.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * based on:
 * @see https://github.com/devboosts/dynamic-component-loader
 * Date: 08/24/2018
 * (c): 2018
 */
import {ComponentFactory, Inject, Injectable, Injector, NgModuleFactoryLoader} from "@angular/core";
import {DYNAMIC_COMPONENT_MANIFESTS, DYNAMIC_CONTENT, DynamicComponentManifest} from "./dynamic-component-manifest";
import {Observable, throwError as ObservableThrow, from as ObservableFromPromise} from "rxjs";

@Injectable()
export class DynamicComponentLoaderService {

  constructor(
    @Inject(DYNAMIC_COMPONENT_MANIFESTS) private manifests: DynamicComponentManifest[],
    private loader: NgModuleFactoryLoader,
    private injector: Injector
  ) {
  }

  getComponentFactory<T>(componentId: string, injector?: Injector): Observable<ComponentFactory<T>> {
    const manifest = this.manifests
      .find(m => m.componentId === componentId);

    if (!manifest) {
      return ObservableThrow(`DynamicComponentLoader: Unknown componentId "${componentId}"`);
    }

    const loadPromise = this.loader.load(manifest.loadChildren)
      .then(ngModuleFactory => {
        const moduleRef = ngModuleFactory.create(injector || this.injector);
        const dynamicComponentType = moduleRef.injector.get(DYNAMIC_CONTENT);

        if (!dynamicComponentType) {
          throw new Error(
            `DynamicComponentLoader: Dynamic module for componentId "${componentId}" does not contain DYNAMIC_COMPONENT as a provider.`,
          );
        }

        return moduleRef.componentFactoryResolver.resolveComponentFactory<T>(dynamicComponentType);
      });

    return ObservableFromPromise(loadPromise);
  }

}
