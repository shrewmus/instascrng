/**
 * @file dynamic-component-loader.module.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * based on:
 * @see https://github.com/devboosts/dynamic-component-loader
 * Date: 08/24/2018
 * (c): 2018
 */
import {
  ANALYZE_FOR_ENTRY_COMPONENTS,
  ModuleWithProviders,
  NgModule,
  NgModuleFactoryLoader,
  SystemJsNgModuleLoader,
  Type
} from "@angular/core";
import {DYNAMIC_COMPONENT_MANIFESTS, DYNAMIC_CONTENT, DynamicComponentManifest} from "./dynamic-component-manifest";
import {ROUTES} from "@angular/router";

@NgModule()
export class DynamicComponentLoaderModule {
  static forRoot(manifests: DynamicComponentManifest[]): ModuleWithProviders {
    return {
      ngModule: DynamicComponentLoaderModule,
      providers: [
        {provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader},
        {provide: ROUTES, useValue: manifests, multi: true},
        {provide: DYNAMIC_COMPONENT_MANIFESTS, useValue: manifests}
      ]
    }
  }

  static forChild(component: Type<any>): ModuleWithProviders {
    return {
      ngModule: DynamicComponentLoaderModule,
      providers: [
        {provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: component, multi: true},
        {provide: ROUTES, useValue: [], multi: true},
        {provide: DYNAMIC_CONTENT, useValue: component}
      ]
    }
  }
}
