/**
 * @file dynamic-component-manifest.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * based on:
 * @see https://github.com/devboosts/dynamic-component-loader
 * Date: 08/24/2018
 * (c): 2018
 */
import {InjectionToken} from "@angular/core";

export const DYNAMIC_CONTENT = new InjectionToken<any>('DYNAMIC_CONTENT');

export const DYNAMIC_COMPONENT_MANIFESTS = new InjectionToken<any>('DYNAMIC_COMPONENT_MANIFESTS');

export interface DynamicComponentManifest {
  componentId: string;
  path: string;
  loadChildren: string;
  data: any;
  preload: boolean;
}
