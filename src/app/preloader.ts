/**
 * @file preloader.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 08/24/2018
 * (c): 2018
 */
import {PreloadingStrategy, Route} from "@angular/router";
import {Observable, of} from "rxjs";


export class ShrPreloader implements PreloadingStrategy {
  preload(route: Route, load: () => Observable<any>): Observable<any> {
    return route.data && route.data.preload ? load() : of(null)
  }
}
